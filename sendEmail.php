<?php

$recaptchaKey = '6Lch-RoTAAAAAH5pdy4wU4KdzCs6jtlnnRwo4BfD';
$recaptchaSecret = '6Lch-RoTAAAAABvwd4Ujvb4RBU4KiROrvs_6mPUM';
$recaptchaResponse = trim($_POST['g-recaptcha-response']);

$remoteIp = $_SERVER['REMOTE_ADDR'];


require_once 'vendor/autoload.php';
$recaptcha = new \ReCaptcha\ReCaptcha($recaptchaSecret);
$resp = $recaptcha->verify($recaptchaResponse, $remoteIp);
if (!$resp->isSuccess()) {
    die;
}else{
    echo 'Recaptcha success';
}

// Check allt he POST vars
$telnr = isset($_POST['telnr']) && !empty($_POST['telnr']) ? trim($_POST['telnr']) : '';
$postcode = isset($_POST['postcode']) && !empty($_POST['postcode']) ? trim($_POST['postcode']) : '';
$adres = isset($_POST['adres']) && !empty($_POST['adres']) ? trim($_POST['adres']) : '';
$lastName = isset($_POST['lastName']) && !empty($_POST['lastName']) ? trim($_POST['lastName']) : '';
$firstName = isset($_POST['firstName']) && !empty($_POST['firstName']) ? trim($_POST['firstName']) : '';
$mobile = isset($_POST['mobile']) && !empty($_POST['mobile']) ? trim($_POST['mobile']) : '';
$email = isset($_POST['email']) && !empty($_POST['email']) ? trim($_POST['email']) : '';
$dob = isset($_POST['dob']) && !empty($_POST['dob']) ? trim($_POST['dob']) : '';
$education = isset($_POST['education']) && !empty($_POST['education']) ? trim($_POST['education']) : '';
$experience = isset($_POST['experience']) && !empty($_POST['experience']) ? trim($_POST['experience']) : '';
$motivation = isset($_POST['motivation']) && !empty($_POST['motivation']) ? trim($_POST['motivation']) : '';
$location = isset($_POST['location']) && !empty($_POST['location']) ? trim($_POST['location']) : '';
$cv = isset($_POST['cv']) && !empty($_POST['cv']) ? trim($_POST['cv']) : '';

//$site_owners_email = 'jobs@we-say-so.com'; // Replace this with your own email address
$site_owners_email = 'pepijn@graciousstudios.nl'; // Replace this with your own email address

// Setup the mail content
$msg = "<html><body style='font-family:Arial,sans-serif;'>";
$msg .= "<h2 style='font-weight:bold;border-bottom:1px dotted #ccc;'>VACATURE</h2>\r\n";
$msg .= "<p><strong>From:</strong> " . $firstName . " " . $lastName . "</p>\r\n";
$msg .= "<p><strong>Phone:</strong> " . $phone . "</p>\r\n";
$msg .= "<p><strong>Message:</strong> <br /> " . $message . " </p>";
$msg .= "</body></html>";

// Add mail headers
$headers = "From: " . $firstName . " " . $lastName . " \r\n";
$headers .= 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

if (empty($firstName)) {
    $error['firstName'] = "Please enter your first name";
}

if (empty($lastName)) {
    $error['lastName'] = "Please enter your last name";
}

if (empty($phone)) {
    $error['phone'] = "Please enter your phone";
}

if (empty($message)) {
    $error['message'] = "Please leave a comment.";
}

if (empty($subject)) {
    $error['subject'] = "Please leave a subject.";
}

if (!$error) {
    $mail = mail($site_owners_email, $subject, $msg, $headers);

    echo "<div class='success'>" . $firstName . " " . $lastName . ", we've received your email. We'll be in touch with you as soon as possible! </div>";
} # end if no error
else {

    foreach ($error as $er) {
        echo '<div class="error">' . $er . '</div>';
    }
} # end if there was an error sending

die;

?>