<?php
require_once 'vendor/autoload.php';

if(isset($_POST['action']) && $_POST['action']=='submit')   {

    $toEmail = 'jobs@we-say-so.com';
//    $toEmail = 'pepijn@graciousstudios.nl';

    $aErrors = array();
    $aRequired = array(
        'c_firstname',
        'c_lastname',
        'c_email',
        'c_adres',
        'c_postcode',
        'c_telnr',
        'c_location',
        'c_gender',
        'c_hours',
        'c_motivation',
        'c_education',
        'c_gender',
        'c_dob',
        'c_city'
    );

    $aTranslate = array(
        'dob'        => 'Geboortedatum',
        'firstname'  => 'Voornaam',
        'lastname'   => 'Achternaam',
        'telnr'      => 'Telefoon',
        'mobile'     => 'Mobiel',
        'education'  => 'Opleiding',
        'motivation' => 'Motivatie',
        'location'   => 'Locatie',
        'gender'     => 'Geslacht',
        'email'      => 'E-mailadres',
        'adres'      => 'Adres',
        'postcode'   => 'Postcode',
        'hours'      => 'Aantal uren per week',
        'city' => 'Woonplaats'
    );

    // RECAPTCHA
    $recaptchaSecret = '6Lch-RoTAAAAABvwd4Ujvb4RBU4KiROrvs_6mPUM';
    $recaptchaResponse = $_POST['g-recaptcha-response'];
    $remoteIp = $_SERVER['REMOTE_ADDR'];
    $recaptcha = new \ReCaptcha\ReCaptcha($recaptchaSecret, new \ReCaptcha\RequestMethod\SocketPost());

    $resp = $recaptcha->verify($recaptchaResponse, $remoteIp);
    if (!$resp->isSuccess()) {
        $aErrors[] = 'Kon niet reCaptcha valideren';
    }else {
        // Check all the POST vars
        $c_gender = isset($_POST['gender']) && !empty($_POST['gender']) ? trim($_POST['gender']) : '';
        $c_dob = isset($_POST['dob']) && !empty($_POST['dob']) ? trim($_POST['dob']) : '';
        $c_firstname = isset($_POST['firstName']) && !empty($_POST['firstName']) ? trim($_POST['firstName']) : '';
        $c_lastname = isset($_POST['lastName']) && !empty($_POST['lastName']) ? trim($_POST['lastName']) : '';
        $c_email = isset($_POST['email']) && !empty($_POST['email']) ? trim($_POST['email']) : '';
        $c_telnr = isset($_POST['telnr']) && !empty($_POST['telnr']) ? trim($_POST['telnr']) : '';
        $c_mobile = isset($_POST['mobile']) && !empty($_POST['mobile']) ? trim($_POST['mobile']) : '';
        $c_adres = isset($_POST['adres']) && !empty($_POST['adres']) ? trim($_POST['adres']) : '';
        $c_postcode = isset($_POST['postcode']) && !empty($_POST['postcode']) ? trim($_POST['postcode']) : '';
        $c_education = isset($_POST['education']) && !empty($_POST['education']) ? trim($_POST['education']) : '';
        $c_experience = isset($_POST['experience']) && !empty($_POST['experience']) ? trim($_POST['experience']) : '';
        $c_motivation = isset($_POST['motivation']) && !empty($_POST['motivation']) ? trim($_POST['motivation']) : '';
        $c_location = isset($_POST['location']) && !empty($_POST['location']) ? trim($_POST['location']) : '';
        $c_hours = isset($_POST['hours']) && !empty($_POST['hours']) ? trim($_POST['hours']) : '';
        $c_city = isset($_POST['city']) && !empty($_POST['city']) ? trim($_POST['city']) : '';

        if(isset($_FILES['cv']) && isset($_FILES['cv']['tmp_name']))    {
            $fileLocation = $_FILES['cv']['tmp_name'];
            $fileName = $_FILES['cv']['name'];
        }


        foreach($aRequired as $required)    {

            if(!isset($$required) || empty($$required)) {
                $requiredName = substr($required,2,strlen($required));
                $requiredName = $aTranslate[$requiredName];
                $aErrors[] = $requiredName . ' is verplicht';
            }
        }
        // Collect all errors
        if (empty($aErrors)) {

            $msgTxt = 'Solicitatie' . PHP_EOL;
            $msgHtml = "<html><body style='font-family:Arial,sans-serif;'>";
            $msgHtml .= "<h2 style='font-weight:bold;border-bottom:1px dotted #ccc;'>SOLLICITATIE VACATURE</h2>\r\n";

            $allDefinedVars = get_defined_vars();

            foreach($allDefinedVars as $definedVarKey=>$definedVarValue) {
                if(substr($definedVarKey, 0,2)=='c_')   {
                    $labelShort = substr($definedVarKey, 2,strlen($definedVarKey));
                    $label = isset($aTranslate[$labelShort]) ? $aTranslate[$labelShort] : $labelShort;
                    $label = ucfirst($label);
                    $msgHtml .= '<p><strong>' . $label .':</strong> ' . $definedVarValue . '</p>';
                    $msgTxt .= $label . ': ' . $definedVarValue;
                }
            }

            $msgHtml .= "</body></html>";

            // Create the Transport
            $mailer = Swift_Mailer::newInstance(Swift_MailTransport::newInstance());

            $message = Swift_Message::newInstance()
                ->setSubject('Solicitatie voor vacature - ' . $c_firstname . ' ' . $c_lastname)
                ->setFrom($c_email)
                ->setTo($toEmail)
                ->setBody($msgTxt)
                ->addPart($msgHtml, 'text/html')
            ;
            if(isset($fileLocation) && !empty($fileLocation))    {
                $message->attach(Swift_Attachment::fromPath($fileLocation)->setFilename($fileName));
            }
            $sent = $mailer->send($message);

        }

    }
}

if(!empty($aErrors))    {
    sort($aErrors);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>We-Say-So | Project manager</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href='vendor/font-awesome/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Playfair+Display:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,700,300' rel='stylesheet' type='text/css'>
    <link href="fonts/Stroke-Gap-Icons-Webfont/style.css" rel="stylesheet" type="text/css"/>

    <!-- components -->
    <link href="vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="vendor/owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css"/>
    <link href="vendor/slider-pro/dist/css/slider-pro.min.css" rel="stylesheet" type="text/css"/>
    <link href="vendor/slick-carousel/slick/slick.css" rel="stylesheet" type="text/css"/>
    <link href="vendor/animate.css/animate.min.css" rel="stylesheet" type="text/css"/>

    <link href="css/main.css" rel="stylesheet" type="text/css"/>
    <script src='https://www.google.com/recaptcha/api.js'></script>

</head>
<body data-scrolling-animations="true" class="blue-theme">

<div id="page-preloader"><span class="spinner"></span></div>

<!-- ========================== -->
<!-- Navigation -->
<!-- ========================== -->
<header class="header scrolling-header">
    <nav id="nav" class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container relative-nav-container">
            <a class="toggle-button visible-xs-block" data-toggle="collapse" data-target="#navbar-collapse">
                <i class="fa fa-navicon"></i>
            </a>
            <a class="navbar-brand scroll" href="index.html">
                <img class="normal-logo hidden-xs" src="img/logo-blue.svg" alt="logo" width="134"/>
                <img class="scroll-logo hidden-xs" src="img/logo-dark-blue.svg" alt="logo" width="134"/>
                <img class="scroll-logo visible-xs-block" src="img/logo-blue.svg" alt="logo" width="134"/>
            </a>
        </div>
    </nav>
</header><!--./navigation -->


<!-- ========================== -->
<!-- SERVICES - HEADER -->
<!-- ========================== -->
<section class="top-header header-vacancies services-header with-bottom-effect transparent-effect dark">
    <div class="bottom-effect"></div>
    <div class="header-container wow fadeInUp">
        <div class="header-title">
            <div class="header-icon"><span class=""></span></div>
            <div class="title">VACATURES</div>
        </div>
    </div><!--container-->
</section>


<!-- ABOUT - STEPS  -->
<!-- ========================== -->
<section id="wiezijnwij" class="active steps-section with-icon">
    <div class="container">
        <div class="section-heading">
            <?php if(isset($sent) && $sent): ?>
                <div class="section-title">
                    Bedankt voor uw sollicitatie
                </div>
                <div class="design-arrow"></div>
                <br>
                <div class="section-title">
                    We nemen zo spoedig mogelijk contact op
                </div>
            <?php endif; ?>

            <?php if(!isset($sent) || !$sent): ?>
            <div class="section-title">Versterk ons team</div>
            <div class="design-arrow"></div>
            <br>
            <?php endif; ?>




            <div class="col-md-12 col-md-offset-1 col-sm-12">
                <div class="contact-form">
                    <div class="form-heading">

                    </div>
                    <div id="response">
                        <?php if(!empty($aErrors)): ?>
                            <div class="error">
                                <ul>
                                    <?php foreach($aErrors as $error): ?>
                                        <li><?php echo $error; ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endif;?>
                    </div>
                    <?php if(!isset($sent)): ?>
                    <form method="post" action="" name="contact-form" id="contact-form" enctype="multipart/form-data">

                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" name="firstName" id="firstName" placeholder="VOORNAAM" class="form-control" value="<?php echo isset($_POST['firstName']) ? $_POST['firstName'] : '' ?>"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" name="lastName" id="lastName" placeholder="ACHTERNAAM" class="form-control" value="<?php echo isset($_POST['lastName']) ? $_POST['lastName'] : '' ?>"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select type="text" name="gender" id="gender" placeholder="GESLACHT" class="form-control">
                                        <option value="">GESLACHT</option>
                                        <option value="m" <?php if(isset($_POST['gender']) && $_POST['gender']=='m') echo 'selected="selected"'?>>MAN</option>
                                        <option value="v"<?php if(isset($_POST['gender']) && $_POST['gender']=='v') echo 'selected="selected"'?>>VROUW</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="adres" id="adres" placeholder="ADRES" class="form-control" value="<?php echo isset($_POST['adres']) ? $_POST['adres'] : '' ?>"/>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" name="postcode" id="postcode" placeholder="POSTCODE" class="form-control" value="<?php echo isset($_POST['postcode']) ? $_POST['postcode'] : '' ?>"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" name="city" id="city" placeholder="WOONPLAATS" class="form-control" value="<?php echo isset($_POST['city']) ? $_POST['city'] : '' ?>"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="telnr" id="telnr" placeholder="TELEFOONNUMMER" class="form-control" value="<?php echo isset($_POST['telnr']) ? $_POST['telnr'] : ''?>"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="mobile" id="mobile" placeholder="MOBIEL" class="form-control" value="<?php echo isset($_POST['mobile']) ? $_POST['mobile'] : '' ?>"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="email" id="email" placeholder="E-MAIL ADRES" class="form-control" value="<?php echo isset($_POST['email']) ? $_POST['email'] : '' ?>"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="dob" id="dob" placeholder="GEBOORTEDATUM" class="form-control" value="<?php echo isset($_POST['dob']) ? $_POST['dob'] : ''?>"/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea class="form-control" name="education" id="education" placeholder="OPLEIDINGEN"><?php echo isset($_POST['education']) ? $_POST['education'] : '' ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea class="form-control" name="experience" id="experience" placeholder="WERKERVARING"><?php echo isset($_POST['experience']) ? $_POST['experience'] : '' ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea class="form-control" name="motivation" id="motivation" placeholder="MOTIVATIE"><?php echo isset($_POST['motivation']) ? $_POST['motivation'] : ''?></textarea>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select class="form-control" name="hours" id="hours" placeholder="AANTAL UUR">
                                        <option value="">AANTAL UUR PER WEEK</option>
                                        <option value="minder dan 16 uur" <?php if(isset($_POST['hours']) && $_POST['hours']=='minder dan 16 uur') echo 'selected="selected"'?>>Minder dan 16 uur</option>
                                        <option value="tussen 16 en 24 uur" <?php if(isset($_POST['hours']) && $_POST['hours']=='tussen 16 en 24 uur') echo 'selected="selected"'?>>Tussen 16 en 24 uur</option>
                                        <option value="tussen 24 en 39 uur" <?php if(isset($_POST['hours']) && $_POST['hours']=='tussen 24 en 39 uur') echo 'selected="selected"'?>>Tussen 24 en 39 uur</option>
                                        <option value="fulltime" <?php if(isset($_POST['hours']) && $_POST['hours']=='m') echo 'selected="selected"'?>>Fulltime</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select class="form-control" name="location" id="location" placeholder="LOCATIE">
                                        <option value="">LOCATIE</option>
                                        <option value="Delft" <?php if(isset($_POST['location']) && $_POST['location']=='Delft') echo 'selected="selected"'?>>Delft</option>
                                        <option value="Den Haag" <?php if(isset($_POST['location']) && $_POST['location']=='Den Haag') echo 'selected="selected"'?>>Den Haag</option>
                                        <option value="Rotterdam" <?php if(isset($_POST['location']) && $_POST['location']=='Rotterdam') echo 'selected="selected"'?>>Rotterdam</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <span class="btn btn-default btn-file">
                                        Upload je CV <input type="file" name="cv" id="cv" value="<?php echo isset($_POST['cv']) ? $_POST['cv'] : '' ?>"/>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="hidden" name="action" value="submit"/>
                                    <button type="submit" id="submit" class="btn btn-primary">Verzenden</button>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="g-recaptcha" data-sitekey="6Lch-RoTAAAAAH5pdy4wU4KdzCs6jtlnnRwo4BfD"></div>
                                </div>
                            </div>

                        </div>
                    </form>
                    <?php endif; ?>
                </div>
            </div>

            </center>
        </div>
    </div>


</section>

<!-- ========================== -->
<!-- FOOTER - FOOTER -->
<!-- ========================== -->
<section class="footer-section">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3">
                <h5>Sitemap</h5>
                <div class="row">
                    <div class="col-md-6">
                        <ul class="footer-nav">
                            <li><a href="index.html#wiezijnwij">Diensten</a></li>
                            <li><a href="compliancy.html">Compliancy</a></li>
                            <li><a href="index.html#locaties">Contact Centers</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul class="footer-nav">
                            <li class="active"><a href="index.html#werken">Werken bij WSS</a></li>
                            <br>
                            <a href="https://www.facebook.com/wss.wesayso" target="_blank"><img src="img/facebook.svg" width="30px" height=auto"
                                "alt="Facebook"></a>
                            <a href="https://twitter.com/wesayso_CC" target="_blank"><img src="img/twitter.svg" width="30px" height=auto"
                                "alt="Twitter"></a>


                        </ul>
                    </div>
                </div>

            </div>
            <div class="col-md-3 col-sm-3">
                <h5>Delft</h5>
                <ul class="contacts-list">
                    <li>
                        <p><i class="icon icon-House"></i>Vesteplein 106, 2611 WG</p>
                    </li>
                    <li>
                        <p><i class="icon icon-Phone2"></i>015 - 75 03 913</p>
                    </li>
                    <li>
                        <p><i class="icon icon-Mail"></i><a href="mailto:info@we-say-so.com<">info@we-say-so.com</a></p>

                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3">
                <h5>Den Haag</h5>
                <ul class="contacts-list">
                    <li>
                        <p><i class="icon icon-House"></i>Kettingstraat 2, 2511AN</p>
                    </li>
                    <li>
                        <p><i class="icon icon-Phone2"></i>070 - 75 20 955</p>
                    </li>
                    <li>
                        <p><i class="icon icon-Mail"></i><a href="mailto:info@we-say-so.com<">info@we-say-so.com</a></p>

                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3">
                <h5>Rotterdam</h5>
                <ul class="contacts-list">
                    <li>
                        <p><i class="icon icon-House"></i>Karel Doormanstraat 331, 3012 GH</p>
                    </li>
                    <li>
                        <p><i class="icon icon-Phone2"></i>010 - 84 17 634</p>
                    </li>
                    <li>
                        <p><i class="icon icon-Mail"></i><a href="mailto:info@we-say-so.com<">info@we-say-so.com</a></p>

                    </li>
                </ul>
            </div>
        </div>
    </div>
    </div>
</section>

</footer>

<!-- Scripts -->

<!-- components -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="vendor/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="vendor/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="vendor/owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
<script src="vendor/waypoints/lib/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="vendor/slider-pro/dist/js/jquery.sliderPro.min.js" type="text/javascript"></script>
<script src="vendor/slick-carousel/slick/slick.min.js" type="text/javascript"></script>
<script src="vendor/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.min.js" type="text/javascript"></script>
<script src="vendor/smooth-scroll/smooth-scroll.min.js" type="text/javascript"></script>
<script src="vendor/wow/dist/wow.min.js" type="text/javascript"></script>
<script src="js/modernizr.js" type="text/javascript"></script>
<script src="//maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
<!-- custom scripts -->
<!-- custom scripts -->
<script src="js/contact.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>
<script src="js/map.js" type="text/javascript"></script>

</body>
</html>
